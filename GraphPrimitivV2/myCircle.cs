﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GraphPrimitivV2
{
    class myCircle : myBase
    {
        public int x;
        public int y;
        public int radius;
        public Brush color;
        Ellipse myEllipse;
        Canvas canvas;

        public myCircle()
        {
            x = 10;
            y = 10;
            radius = 5;
            color = Brushes.Black;
            ConfigEclipse();
        }
        public myCircle(Canvas canvas, int x, int y, int radius, Brush color)
        {
            this.x = x;
            this.y = y;
            this.radius = radius;
            this.color = color;
            this.canvas = canvas;
            ConfigEclipse();
        }
        public override void Show()
        {
            Canvas.SetTop(myEllipse, y);
            Canvas.SetLeft(myEllipse, x);
            canvas.Children.Add(myEllipse);
        }
        public override void Hide()
        {
            canvas.Children.Remove(myEllipse);
        }
        public override void Move(int dx, int dy)
        {
            Hide();
            this.x += dx;
            this.y += dy;
            Show();
        }
        public override void SaveInFile(StreamWriter writer)
        {
            writer.WriteLine("SegmentLine: " + x.ToString() + " " + y.ToString() + " "  + radius.ToString() + " " + new BrushConverter().ConvertToString(color));
        }

        private void ConfigEclipse()
        {
            myEllipse = new Ellipse();
            myEllipse.Width = radius;
            myEllipse.Height = radius;
            myEllipse.Fill = color;
        }
    }
}
