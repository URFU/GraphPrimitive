﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphPrimitivV2
{
    class Figure : myBase
    {
        List<myBase> objects;
        public Figure()
        {
            objects = new List<myBase>();
        }
        public void Add(myBase obj)
        {
            objects.Add(obj);
        }
        public override void Show()
        {
            foreach (var item in objects)
                item.Show();
        }
        public override void Hide()
        {
            foreach (var item in objects)
                item.Hide();
        }
        public override void Move(int dx, int dy)
        {
            foreach (var item in objects)
                item.Move(dx, dy);
        }
        public override void SaveInFile (StreamWriter writer)
        {
            foreach (var item in objects)
                item.SaveInFile(writer);
        }
    }
}
