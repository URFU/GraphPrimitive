﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GraphPrimitivV2
{
    class myPoint : myBase
    {
        public int x;
        public int y;
        public int radius;
        public Brush color;
        Ellipse myEllipse;
        Canvas canvas;
        public myPoint()
        {
            x = 0;
            y = 0;
            radius = 5;
            color = Brushes.Black;
            ConfigEclipse();
        }
        public myPoint(Canvas canvas, int x, int y, int radius, Brush color)
        {
            this.x = x;
            this.y = y;
            this.radius = radius;
            this.color = color;
            this.canvas = canvas;
            ConfigEclipse();
        }

        public myPoint(Canvas canvas, int x, int y, int radius)
        {
            this.x = x;
            this.y = y;
            this.radius = radius;
            color = Brushes.Black;
            this.canvas = canvas;
            ConfigEclipse();
        }

        public myPoint(Canvas canvas, int x, int y)
        {
            this.x = x;
            this.y = y;
            radius = 5;
            color = Brushes.Black;
            this.canvas = canvas;
            ConfigEclipse();
        }
        public override void Show()
        {
            Canvas.SetTop(myEllipse, y);
            Canvas.SetLeft(myEllipse, x);
            canvas.Children.Add(myEllipse);
        }
        public override void Hide()
        {
            canvas.Children.Remove(myEllipse);
        }
        public override void Move(int dx, int dy)
        {
            Hide();
            this.x += dx;
            this.y += dy;
            Show();
        }
        public override void SaveInFile(StreamWriter writer)
        {
            writer.WriteLine("Point: " + x.ToString() + " " + y.ToString() + " " + radius.ToString() + new BrushConverter().ConvertToString(color));
        }
        private void ConfigEclipse()
        {
            myEllipse = new Ellipse();
            myEllipse.Width = radius;
            myEllipse.Height = radius;
            myEllipse.Fill = color;
        }
    }
}
