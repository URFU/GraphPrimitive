﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphPrimitivV2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Figure myFig = new Figure();
        public MainWindow()
        {
            InitializeComponent();
            myLineSegment downCarLine = new myLineSegment(CanvasMain, 125, 75, 275, 75, Brushes.Red);   /*линия между колесами*/
            myFig.Add(downCarLine);
            myLineSegment vertCarLineLeft = new myLineSegment(CanvasMain, 125, 75, 125, 20, Brushes.Red);/*левая линия машины*/
            myFig.Add(vertCarLineLeft);
            myLineSegment vertCarLineRight = new myLineSegment(CanvasMain, 275, 75, 275, 35, Brushes.Red);/*правая линия машины*/
            myFig.Add(vertCarLineRight);
            myLineSegment CarLineCapote = new myLineSegment(CanvasMain, 250, 35, 275, 35, Brushes.Red);/*линия капота*/
            myFig.Add(CarLineCapote);
            myLineSegment vertCarLineCapote = new myLineSegment(CanvasMain, 250, 35, 250, 20, Brushes.Red);/*линия капота*/
            myFig.Add(vertCarLineCapote);
            myLineSegment roofLine = new myLineSegment(CanvasMain, 125, 20, 250, 20, Brushes.Red);/*линия крыши*/
            myFig.Add(roofLine);
            myCircle LeftRoll = new myCircle(CanvasMain, 100, 50, 50, Brushes.Green);       /*Левое колесо*/
            myFig.Add(LeftRoll);
            myCircle RightRoll = new myCircle(CanvasMain, 200, 50, 50, Brushes.Green);       /*Правое колесо*/
            myFig.Add(RightRoll);
            myPoint sirena = new myPoint(CanvasMain, 150, 15, 10,Brushes.Blue);
            myFig.Add(sirena);
            myFig.Show();

            FileStream fileCar = new FileStream("car.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(fileCar);
            myFig.SaveInFile(writer);
            writer.Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                myFig.Move(-20, 0);
            }
            else if (e.Key == Key.Right)
            {
                myFig.Move(20, 0);
            }
            else if (e.Key == Key.Up)
            {
                myFig.Move(0, -20);
            }
            else if (e.Key == Key.Down)
            {
                myFig.Move(0, 20);
            }
        }
    }
}
