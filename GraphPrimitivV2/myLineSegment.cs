﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GraphPrimitivV2
{
    class myLineSegment : myBase
    {
        public int x1, x2;
        public int y1, y2;
        public Brush color;
        Line myLine;
        Canvas canvas;

        public myLineSegment()
        {
            x1 = y1 = 0;
            x2 = y2 = 10;
            ConfigLine();
        }
        public myLineSegment(Canvas canvas, int x1, int y1, int x2, int y2)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.canvas = canvas;
            ConfigLine();
        }
        public myLineSegment(Canvas canvas, int x1, int y1, int x2, int y2, Brush color)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.color = color;
            this.canvas = canvas;
            ConfigLine();
        }
        public override void Show ()
        {
            canvas.Children.Add(myLine);
        }
        public override void Hide()
        {
            canvas.Children.Remove(myLine);
        }
        public override void Move(int dx, int dy)
        {
            Hide();
            this.x1 += dx;
            this.y1 += dy;
            this.x2 += dx;
            this.y2 += dy;
            ConfigLine();
            Show();
        }
        public override void SaveInFile(StreamWriter writer)
        {
            writer.WriteLine("SegmentLine: " + x1.ToString() + " " + y1.ToString() + " " + x2.ToString() + " " + y2.ToString() + " " + new BrushConverter().ConvertToString(color));
        }
        private void ConfigLine()
        {
            myLine = new Line();
            myLine.Stroke = color;
            myLine.X1 = x1;
            myLine.X2 = x2;
            myLine.Y1 = y1;
            myLine.Y2 = y2;
            myLine.HorizontalAlignment = HorizontalAlignment.Left;
            myLine.VerticalAlignment = VerticalAlignment.Center;
            myLine.StrokeThickness = 2;
        }
    }
}
