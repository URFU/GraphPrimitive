﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GraphPrimitivV2
{
    abstract class myBase
    {
        abstract public void Show();
        abstract public void Hide();
        abstract public void Move(int dx, int dy);
        abstract public void SaveInFile(StreamWriter writer);
    }
}
